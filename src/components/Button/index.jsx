import { useContext } from "react"
import { GlobalContext } from "../../contexts/GlobalContext"


export const Button = () =>{
    const {dispatch} = useContext(GlobalContext)
    return(
      <button onClick={()=> dispatch({type: "increment"}) }>+</button>
    )
  }