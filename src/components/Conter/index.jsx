import { useContext } from "react"
import { GlobalContext } from "../../contexts/GlobalContext"


export const Conter = ()=>{
  
    const { state } = useContext(GlobalContext)
  
    return(
      <h2 >
        conter: {state.number}
      </h2>
    )
  }
  