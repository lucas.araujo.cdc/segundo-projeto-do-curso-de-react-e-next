import { Button } from "../Button"
import { Heading } from "../Heading"
import { Conter } from "../Conter"

export const Header = ()=>{

    return(
      <header className="App-header">
        <Heading/>
            <Conter/> 
        <Button/>
      </header>
    )
  }