import { useContext } from "react"
import { GlobalContext } from "../../contexts/GlobalContext"

export const Heading = ()=>{
  
    const {state} = useContext(GlobalContext)
   
    return(
      <h1 >
        {state.title}
      </h1>
    )
  }