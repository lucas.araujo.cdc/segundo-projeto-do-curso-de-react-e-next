import { useContext, useEffect, useRef } from "react"
import { PostsContext } from "../../contexts/PostsProviders/context"
import { loadPosts } from "../../contexts/PostsProviders/actions"
import { CounterContext } from "../../contexts/ExempleProvider/context"
import { decrementCounter, incrementCounter } from "../../contexts/ExempleProvider/action"


export const Posts = () => {

    const { postsState , postsDispach } = useContext(PostsContext)
    const isMounted = useRef(true)

    const { counterState, counterDispatch } = useContext(CounterContext)

    useEffect(()=> {
            loadPosts(postsDispach).then((dispatch)=>{
                if(isMounted.current){
                dispatch()
            }
            return ()=> { 
                isMounted.current = false
            }
            })   
    }, [postsDispach])

    
    return( 
        <div>
            <button onClick={()=> incrementCounter(counterDispatch)}>Counter {counterState.counter} +</button>
            <button onClick={()=> decrementCounter(counterDispatch)}>Counter {counterState.counter} -</button>
            <h1>
                Posts 
            </h1>
            {postsState.loading && <strong>Carregando posts...</strong>}
            {postsState.posts?.map((posts) => <p key={posts.id}>{posts.title}</p>)}
        </div>
    )

}