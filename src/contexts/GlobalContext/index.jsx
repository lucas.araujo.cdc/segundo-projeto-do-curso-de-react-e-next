import { createContext, useReducer} from "react"

const  obj2 = {
    title: "Ola mundo",
    body: "Teste de contexto",
    number: 0
}
 
export const GlobalContext = createContext()

export const reducer = (state, action) =>{

    if(action.type === "increment"){
        return({...state, number: state.number +1})
    }
    return {...state}
}

export const GlobalContextProvider = ({children}) =>{

    const [state, dispatch] = useReducer(reducer, obj2)

    return(
    <GlobalContext.Provider value={{ state, dispatch}}>
        {children}
    </GlobalContext.Provider>)
    

}