import * as type from "./types"

export const loadPosts = async (dispatch) =>{ 

    dispatch({type: type.POSTS_LOADING, loading:true})

    
    const postsRaw = await fetch("https://jsonplaceholder.typicode.com/posts")
    const posts = await postsRaw.json()
    return()=> dispatch({type: type.POSTS_SUCCESS, payload: posts , })


}