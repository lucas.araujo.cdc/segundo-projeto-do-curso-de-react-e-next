import P from "prop-types";
import { reducer } from "./reducer.js";
import { data } from "./data.js";
import { PostsContext } from "./context.js";
import { useReducer } from "react";

export const PostsProvider = ({children}) =>{
    const [postsState, postsDispach ] = useReducer(reducer, data);

    return (
    <PostsContext.Provider value={{ postsState , postsDispach }}>
        {children}
    </PostsContext.Provider>
    ) 
}

PostsProvider.prototype = {
    children: P.node.isRequired,
}