import { Posts } from '../components/Posts';
import { CounterProvider } from '../contexts/ExempleProvider';
import { PostsProvider } from '../contexts/PostsProviders';
import './App.css';
import React from 'react';



function App() {

  return (
    <CounterProvider>
      <PostsProvider>
        <div className="App">
          <div className='App-header'>
            <Posts/>
          </div>
        </div>
      </PostsProvider>
    </CounterProvider>
  );
}

export default App;
